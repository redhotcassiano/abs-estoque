import Vue from 'vue'
import VueRouter from 'vue-router'
import auth from './auth/login'
import { LocalStorage } from 'quasar'

Vue.use(VueRouter)

function load (component) {
  // '@' is aliased to src/components
  return () => import(`@/${component}.vue`)
}

export default new VueRouter({
  /*
   * NOTE! VueRouter "history" mode DOESN'T works for Cordova builds,
   * it is only to be used only for websites.
   *
   * If you decide to go with "history" mode, please also open /config/index.js
   * and set "build.publicPath" to something other than an empty string.
   * Example: '/' instead of current ''
   *
   * If switching back to default "hash" mode, don't forget to set the
   * build publicPath back to '' so Cordova builds work again.
   */

  mode: 'hash',
  scrollBehavior: () => ({ y: 0 }),
  routes: [{ path: '/', component: load('Hello'), name: 'dashboard.index', children: [{ path: '/', component: load('Charts') }, { path: '/produtos', component: load('Products/List'), children: [ { path: 'add', component: load('Products/Add') } ] }, { path: '/produtos/:id', component: load('Products/View'), children: [ { path: 'edit', component: load('Products/Edit') } ] }], beforeEnter: checkAuth },
    // Always leave this last one
    { path: '/login', name: 'login.index', component: load('Auth/Login') },
    { path: '*', component: load('Error404') } // Not found
  ]
})

function checkAuth (to, from, next) {
  if (to.name === 'dashboard.index' && auth.user.authenticated) {
    next('/')
  }
  else if (!LocalStorage.get.item('id_token') && to.name !== 'login.index') {
    console.log('not logged')
    next('/login')
  }
  else {
    next()
  }
}
