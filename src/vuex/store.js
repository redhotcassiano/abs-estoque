import Products from './modules/products'

export default {
  modules: {
    products: Products
  }
}
