import axios from 'axios'
import { LocalStorage, SessionStorage, Platform } from 'quasar'

axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
axios.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded'

if (LocalStorage.get.item('id_token') != null) {
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + LocalStorage.get.item('id_token')
}

if (Platform.is.desktop) {
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + SessionStorage.get.item('id_token')
}

export default {
  state: {
    list: [],
    one: {}
  },
  mutations: {
    update (state, config) {
      state[config.state] = config.data
    }
  },
  actions: {
    productsList (context) {
      return axios.get('http://13.59.235.27/api/products')
        .then((res) => {
          context.commit('update', {
            state: 'list',
            data: res.data.data
          })
        })
    },
    productsGet (context, id) {
      return axios.get('http://13.59.235.27/api/products/' + id)
        .then((res) => {
          context.commit('update', {
            state: 'one',
            data: res.data
          })
        })
    },
    productsInsert (context, data) {
      return axios.post('http://13.59.235.27/api/products', data)
    },
    productsUpdate (context, config) {
      return axios.put('http://13.59.235.27/api/products/' + config.id, config.data)
    },
    productsDelete (context, id) {
      return axios.delete('http://13.59.235.27/api/products/' + id)
    }
  }
}
